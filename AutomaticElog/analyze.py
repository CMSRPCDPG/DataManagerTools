#!/usr/bin/env python
import os, sys
import json

if len(sys.argv) < 3:
	print "Usage: %s ERA DATASET RUNNUMBER" % sys.argv[0]
	sys.exit()

era = sys.argv[1]
datasetName = sys.argv[2]
runNumber = int(sys.argv[3])

certType = 'Collisions'
if 'Cosmic' in datasetName: certType = 'Cosmics'
eraType = era[-2:]

print "@@ Connecting to DQM @@"

## Open DQM root file
sys.argv.append("-b")
from ROOT import *

sys.path.append('scripts')
from browseDQM import *
dqmJSON = DQMJSON()
dqmJSON.loadRuns('/%s/%s.*' % (datasetName, era))
if runNumber not in dqmJSON.runs:
    print "!! Cannot find run %d in the list" % runNumber
    print " Recently available runs are: ",
    for i, x in enumerate(reversed(sorted(dqmJSON.runs.keys()))):
        if i > 25: break
        print x,
    sys.exit()
datasetFullName = dqmJSON.runs[runNumber]

def getInfo(subpath):
    return dqmJSON.getObjectInfo(runNumber, "RPC/"+subpath)

def getME(subpath):
    return dqmJSON.getObjectJSON(runNumber, "RPC/"+subpath)

nEvent = int(getInfo("EventInfo/iEvent")['value'])
nRPCEvents = getInfo("AllHits/RPCEvents")['nentries']
nLS = int(getInfo("DCSInfo/rpcHV")['value'])
nFATAL = getInfo("FEDIntegrity/FEDFatal")['nentries']

clsBarrel = getInfo("Muon/SummaryHistograms/ClusterSize_Barrel")['stats']['x']['mean']
clsEndcapP = getInfo("Muon/SummaryHistograms/ClusterSize_Endcap+")['stats']['x']['mean']
clsEndcapM = getInfo("Muon/SummaryHistograms/ClusterSize_Endcap-")['stats']['x']['mean']
clsEndcap = (clsEndcapP+clsEndcapM)/2;
nClBarrel = getInfo("Muon/SummaryHistograms/NumberOfClusters_Barrel")['stats']['x']['mean']
nClEndcapP = getInfo("Muon/SummaryHistograms/NumberOfClusters_Endcap+")['stats']['x']['mean']
nClEndcapM = getInfo("Muon/SummaryHistograms/NumberOfClusters_Endcap-")['stats']['x']['mean']
multBarrel = getInfo("AllHits/SummaryHistograms/Multiplicity_Barrel")['stats']['x']['mean']
multEndcapP = getInfo("Muon/SummaryHistograms/Multiplicity_Endcap+")['stats']['x']['mean']
multEndcapM = getInfo("Muon/SummaryHistograms/Multiplicity_Endcap-")['stats']['x']['mean']
multEndcap = (multEndcapP+multEndcapM)/2

## Chamber quality
nQB, nBxB = 0, 0
barrelNSt, barrelNCh, barrelPdd, barrelFdd, barrelBsh = [], [], [], [], []
barrelBx = []
for w in range(-2, 3):
  hQ  = getME("Muon/SummaryHistograms/RPCChamberQuality_Distribution_Wheel%d" % w)
  nQB += hQ['stats']['entries']
  cnts = hQ['bins']['content']
  barrelNSt.append(cnts[2])
  barrelNCh.append(cnts[3])
  barrelPdd.append(cnts[4])
  barrelFdd.append(cnts[5])
  barrelBsh.append(cnts[6])

  me = getInfo("Muon/SummaryHistograms/BxDistribution_Wheel_%d" % w)
  barrelBx.append(me['stats']['x']['mean'])
  nBxB += me['nentries']

nQE, nBxE = 0, 0
endcapNSt, endcapNCh, endcapPdd, endcapFdd, endcapBsh = [], [], [], [], []
endcapBx = []
for d in range(-4, 5):
  if d == 0: continue
  hQ  = getME("Muon/SummaryHistograms/RPCChamberQuality_Distribution_Disk%d" % d)
  nQE += hQ['stats']['entries']
  cnts = hQ['bins']['content']
  endcapNSt.append(cnts[2])
  endcapNCh.append(cnts[3])
  endcapPdd.append(cnts[4])
  endcapFdd.append(cnts[5])
  endcapBsh.append(cnts[6])


  me = getInfo("Muon/SummaryHistograms/BxDistribution_Disk_%d" % d)
  endcapBx.append(me['stats']['x']['mean'])
  nBxE += me['nentries']

"""
## RecHit multiplicity
for w in range(-2, 3):
  hO = f.Get(runSummaryPath+("Muon/SummaryHistograms/Occupancy_Roll_vs_Sector_Wheel_%d" % w))
  c = TCanvas("cOccupancyW%d" % w, "Occupancy W%d" % w, 600, 500)
  hO.Draw("COLZ")
  c.Print("cache/%d/%s.png" % (runNumber, c.GetName()))
hO = f.Get(runSummaryPath+"Muon/SummaryHistograms/Occupancy_for_Barrel")
c = TCanvas("cOccupancyB", "Occupancy for Barrel", 600, 500)
hO.Draw("COLZ")
c.Print("cache/%d/%s.png" % (runNumber, c.GetName()))

for d in range(-4, 5):
  hO = f.Get(runSummaryPath+("Muon/SummaryHistograms/Occupancy_Roll_vs_Sector_Endcap_%d" % d))
  if hO == None: continue
  c = TCanvas("cOccupancyE%d" % d, "Occupancy E%d" % d, 600, 500)
  hO.Draw("COLZ")
  c.Print("cache/%d/%s.png" % (runNumber, c.GetName()))
hO = f.Get(runSummaryPath+"Muon/SummaryHistograms/Occupancy_for_Endcap")
c = TCanvas("cOccupancyE", "Occupancy for Endcap", 600, 500)
hO.Draw("COLZ")
c.Print("cache/%d/%s.png" % (runNumber, c.GetName()))
"""

## Analyse NoiseTool
hasNTWeb, hasWBM = False, False
nDisabled, nDead, nEnable, nToDisable = -1, -1, -1, -1
rateBarrel, rateEndcap = -1, -1
if os.path.exists("cache/%d/output_strips.json" % runNumber):
  hasNTWeb = True
  jsStrips = json.loads(open("cache/%d/output_strips.json" % runNumber).read())
  if 'tomask' not in jsStrips: hasNTWeb = False
  nToMask = 0
  stripsToMask = ""
  for roll in jsStrips['tomask']:
      stripsToMask += "\n     Roll %s" % roll
      info = jsStrips['tomask'][roll]
      nToMask += len(info['channels'])
      for c, s, r, t in zip(info['channels'], info['strips'], info['rates'], info['times']):
          stripsToMask += "\n       channel=%s\tstrip=%d\trate=%.3f (Hz)\tduration=%.2f (s)" % (c, s, r, t)

if os.path.exists("cache/%d/Brate.json" % runNumber):
  hasWBM = True
  jsBRate = json.loads(open("cache/%s/Brate.json" % runNumber).read())
  jsBDisabled = json.loads(open("cache/%s/Bdisabled.json" % runNumber).read())
  jsBDead = json.loads(open("cache/%s/BdeadStrip.json" % runNumber).read())
  jsBToDisable = json.loads(open("cache/%s/BtoDisable.json" % runNumber).read())
  jsBToEnable = json.loads(open("cache/%s/BtoEnable.json" % runNumber).read())

  jsERate = json.loads(open("cache/%s/Erate.json" % runNumber).read())
  jsEDisabled = json.loads(open("cache/%s/Edisabled.json" % runNumber).read())
  jsEDead = json.loads(open("cache/%s/EdeadStrip.json" % runNumber).read())
  jsEToDisable = json.loads(open("cache/%s/EtoDisable.json" % runNumber).read())
  jsEToEnable = json.loads(open("cache/%s/EtoEnable.json" % runNumber).read())

  rateBarrel, rateEndcap = jsBRate['mean'], jsERate['mean']
  nDisabled = jsBDisabled['sum'] + jsEDisabled['sum']
  nDead = jsBDead['sum'] + jsEDead['sum']
  nEnable = jsBToEnable['sum'] + jsEToEnable['sum']
  nToDisable = jsBToDisable['sum'] + jsEToDisable['sum']

## Write elog
runFlag = ""
badReasons = []
if certType == "Cosmics":
    if nFATAL != 0: badReasons.append("(nFATAL=%d) != 0" % nFATAL)

    if nRPCEvents < 10000:
        runFlag = "(short run)"
        if (nEnable+nDisabled) >= 0.10*68280: badReasons.append("(nEnable+nDisabled)/68280 >= 10%%, nEnable = %d, nDisabled = %d" % (nEnable, nDisabled))

    else:
        if clsBarrel >= 3.0: badReasons.append("(clsBarrel=%f) >= 3.0" % clsBarrel)
        if nClBarrel >= 5.0: badReasons.append("(nClBarrel=%f) >= 5.0" % nClBarrel)
        if multBarrel >= 6.0: badReasons.append("(multBarrel=%f) >= 6.0" % multBarrel)
        if (nEnable+nDisabled) >= 0.05*68280:
            badReasons.append("(nEnable+nDisabled)/68280 >= 5%%, nEnable = %d, nDisabled = %d" % (nEnable, nDisabled))

elif certType == "Collisions":
    if nFATAL != 0: badReasons.append("(nFATAL=%d) != 0" % nFATAL)

    if nLS <= 15:
        runFlag = "(very short run)"
        #if len(badReasons) == 0: runFlag = "NOTBAD,TO BE DOUBLE CHECKED " + runFlag
        #else: runFlag = "BAD" + runFlag

    elif nLS < 200:
        if (nEnable+nDisabled) >= 0.10*(68280+41472):
            badReasons.append("(nEnable+nDisabled)/(68280+41472) >= 10%%, nEnable = %d, nDisabled = %d" % (nEnable, nDisabled))

        for i, bx in enumerate(barrelBx):
            wheel = i-2
            if abs(bx) > 0.11:
                badReasons.append("|Bunch crossing| of wheel %d = %.3f, out of range [-0.11,0.11]. Please check Bx distribution and synchronization issue" % (bx, wheel))
        for i, bx in enumerate(endcapBx):
            disk = i-4
            if abs(bx) > 0.11:
                badReasons.append("|Bunch crossing| of disk %d = %.3f, out of range [-0.11,0.11]. Please check Bx distribution and synchronization issue" % (bx, disk))

        runFlag = "(short run)"

    else:
        if (nEnable+nDisabled) >= 0.05*(68280+41472):
            badReasons.append("(nEnable+nDisabled)/(68280+41472) >= 5%%, nEnable = %d, nDiabled = %d" % (nEnable, nDisabled))
        nBadState = sum(barrelNSt)+sum(barrelNCh)+sum(barrelPdd)+sum(barrelFdd)+sum(barrelBsh) \
                  + sum(endcapNSt)+sum(endcapNCh)+sum(endcapPdd)+sum(endcapFdd)+sum(endcapBsh)
        if nBadState >= 0.1*(nQB+nQE): badReasons.append("nBadState = %d >= 10%%" % nBadState)
        if clsBarrel >= 3.0: badReasons.append("clsBarrel = %f >= 3.0" % clsBarrel)
        if clsEndcap >= 3.0: badReasons.append("clsEndcap = %f >= 3.0" % clsEndcap)
        if nClBarrel > 5.0: badReasons.append("nClBarrel = %f > 5.0" % nClBarrel)
        if nClEndcapP > 5.0: badReasons.append("nClEndcap+ = %f > 5.0" % nClEndcapP)
        if nClEndcapM > 5.0: badReasons.append("nClEndcap- = %f > 5.0" % nClEndcapM)
        if multBarrel > 5.0: badReasons.append("multBarrel = %f > 5.0" % multBarrel)
        if multEndcap > 5.0: badReasons.append("multBarrel = %f > 5.0" % multEndcap)

        for i, bx in enumerate(barrelBx):
            wheel = i-2
            if abs(bx) > 0.11:
                badReasons.append("|Bunch crossing| of wheel %d = %.3f, out of range [-0.11,0.11]. Please check Bx distribution and synchronization issue" % (bx, wheel))
        for i, bx in enumerate(endcapBx):
            disk = i-4
            if abs(bx) > 0.11:
                badReasons.append("|Bunch crossing| of disk %d = %.3f, out of range [-0.11,0.11]. Please check Bx distribution and synchronization issue" % (bx, disk))

        runFlag = "(long run)"

fLog = open("cache/%d/elog.txt" % runNumber, "w")
print>>fLog, "Run%d %s%s is GOOD/BAD %s" % (runNumber, certType, eraType, runFlag)
print>>fLog, ("="*100)
print>>fLog, "     Run        Type               RPC Flag             Events"
print>>fLog, "  %d        %s        GOOD/BAD %s                 %d" % (runNumber, certType, runFlag, nEvent)
print>>fLog, ("="*100), "\n\n"
print>>fLog, "***** Using Offline DQM %s" % datasetFullName
print>>fLog, "      Offline DQM : https://cmsweb.cern.ch/dqm/offline/start?runnr=%d;dataset=%s;sampletype=offline_data;workspace=RPC;root=RPC/Layouts" % (runNumber, datasetFullName)
#print>>fLog, "      Online DQM : https://cmsweb.cern.ch/dqm/online/start?runnr=%d;dataset=/Global/Online/ALL;sampletype=online_data;workspace=RPC;root=RPC/Layouts" % (runNumber)
print>>fLog, "*"*40
print>>fLog
print>>fLog, "*"*40
print>>fLog, "Reason for BAD flag (REMOVE THIS SECTION IF THE RUN IS GOOD):"
if len(badReasons) != 0: print>>fLog, ("\n".join(badReasons))
else:
    print>>fLog, "--- Could not find reason to set BAD with the shifter checklist."
    print>>fLog, "--- Before setting the flag, please double check..."
print>>fLog, "Contact DOC for noise, current or temperature problems."
print>>fLog, "LIST ALL PROBLEMS IN THE SUMMARY ELOG, EVEN MINOR ONES"
print>>fLog, "*"*40
print>>fLog
print>>fLog, "1. # of RPC events = %d\n" % nRPCEvents
print>>fLog, "2. # of FED errors = %d\n" % nFATAL
print>>fLog, "3. Noise tool output"
if hasNTWeb or hasWBM:
  if hasNTWeb: print>>fLog, "   https://rpcbackground.web.cern.ch/rpcbackground/Plots/GR2014/run%d/output_strips.json" % runNumber
  if hasWBM: print>>fLog, "   https://cmswbm.web.cern.ch/cmswbm/cmsdb/servlet/RPCSummary?Run=%d&TopMenu=RPCRunSummary2&TopMenu2=ZeroPage&SubMenu=1" % runNumber
  print>>fLog, "   Disabled: %d    Dead:   %d" % (nDisabled, nDead)
  print>>fLog, "   Enable:   %d  To Disable: %d" % (nEnable, nToDisable)
  print>>fLog, "   Average Barrel Noise Rate: %.2f" % (rateBarrel)
  print>>fLog, "   Average Endcap Noise Rate: %.2f" % (rateEndcap)
  print>>fLog, ""
  if hasNTWeb:
      print>>fLog, "   Total %d strips to mask:" % (nToMask)
      print>>fLog, stripsToMask
else:
  print>>fLog, "NO NOISE TOOL INFORMATION"
print>>fLog, ""
print>>fLog, "4-1. Barrel_RPCChamberQuality_Distribution"
if nQB == 0:
  print>>fLog, "  N/A"
else:
  print>>fLog, "                               W-2  W-1   W0  W+1  W+2"
  print>>fLog, "   Noisy strip                  %d   %d   %d   %d   %d" % tuple(barrelNSt)
  print>>fLog, "   Noisy Chamber                %d   %d   %d   %d   %d" % tuple(barrelNCh)
  print>>fLog, "   Partially Dead               %d   %d   %d   %d   %d" % tuple(barrelPdd)
  print>>fLog, "   Dead                         %d   %d   %d   %d   %d" % tuple(barrelFdd)
  print>>fLog, "   Bad shape                    %d   %d   %d   %d   %d" % tuple(barrelBsh)
print>>fLog, ""
if certType != "Cosmics":
    print>>fLog, "4-2. Endcap_RPCChamberQuality_Distribution"
    if nQE == 0:
      print>>fLog, "  N/A"
    else:
      print>>fLog, "                               D-4  D-3  D-2  D-1  D+1  D+2  D+3  D+4"
      print>>fLog, "   Noisy strip                  %d   %d   %d   %d   %d   %d   %d   %d" % tuple(endcapNSt)
      print>>fLog, "   Noisy Chamber                %d   %d   %d   %d   %d   %d   %d   %d" % tuple(endcapNCh)
      print>>fLog, "   Partially Dead               %d   %d   %d   %d   %d   %d   %d   %d" % tuple(endcapPdd)
      print>>fLog, "   Dead                         %d   %d   %d   %d   %d   %d   %d   %d" % tuple(endcapFdd)
      print>>fLog, "   Bad shape                    %d   %d   %d   %d   %d   %d   %d   %d" % tuple(endcapBsh)
    print>>fLog, ""

print>>fLog, "5-1. BunchCrossing Distribution/Wheel (mean value)"
if nBxB == 0: print>>fLog, " N/A"
else:
  print>>fLog, "                               W-2  W-1   W0  W+1  W+2"
  print>>fLog, "                              %.2f %.2f %.2f %.2f %.2f" % tuple(barrelBx)
print>>fLog, ""
if certType != "Cosmics":
    print>>fLog, "5-2. BunchCrossing Distribution/Disk (mean value)"
    if nBxE == 0: print>>fLog, " N/A"
    else:
      print>>fLog, "                               D-4  D-3  D-2  D-1  D+1  D+2  D+3  D+4"
      print>>fLog, "                              %.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f" % tuple(endcapBx)
    print>>fLog, ""

print>>fLog, "6-1. Average number of clusters per event Barrel = %.1f" % nClBarrel
print>>fLog, "     Average cluster size Barrel                 = %.1f" % clsBarrel
if certType != "Cosmics":
    print>>fLog, "6-2. Average number of clusters per event Endcap = %.1f" % ((nClEndcapP+nClEndcapM)/2)
    print>>fLog, "     Average cluster size Endcap                 = %.1f" % clsEndcap
    print>>fLog, ""
print>>fLog, "7-1. Average number of single hits Barrel        = %.1f" % multBarrel
if certType != "Cosmics": print>>fLog, "7-2. Average number of single hits Endcap        = %.1f" % multEndcap
print>>fLog, ""
print>>fLog, "8-1. Barrel Occupancy  (TO BE CHECKED)"
if certType != "Cosmics": print>>fLog, "8-2. Endcap Occupancy  (TO BE CHECKED)"
fLog.close()

print "\n"
print open("cache/%d/elog.txt" % runNumber).read()
