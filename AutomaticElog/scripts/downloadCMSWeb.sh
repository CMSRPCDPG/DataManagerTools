#!/bin/bash

if [ $# != 2 ]; then
  echo "Download cmsweb pages"
  echo "Usage: $0 COOKIEFILE WEBPAGE OUTPUTFILE"
  exit 1
fi

URL=$1
OUTPUT=$2

BASEURL=`echo $URL | sed 's;cern.ch/.*;cern.ch;g'`
#BASEURL=`echo $URL | sed 's;\?.*;;g'`
#BASEURL=$URL
[ -d cache ] || mkdir cache
COOKIE=cache/cookie-`echo $BASEURL | md5sum | cut -f1 -d' '`

[ -f $COOKIE ] || LD_LIBRARY_PATH="" cern-get-sso-cookie --krb -u $BASEURL -o $COOKIE

## Download NoiseTool pages
CURL="curl -s -L --cookie $COOKIE --cookie-jar $COOKIE"
$CURL $URL -o $OUTPUT
grep -q "404 - Not found" $OUTPUT
NOTFOUND=$?
grep -q "Cern Authentication" $OUTPUT
AUTHFAIL=$?
if [ $NOTFOUND -eq 0 ]; then
  echo "!!! Cannot find web page $URL !!!"
  rm -f $OUTPUT
  exit 404
fi
if [ $AUTHFAIL -eq 0 ]; then
  echo "!!! authentication failure $URL !!!"
  rm -f $OUTPUT
  exit 202
fi
