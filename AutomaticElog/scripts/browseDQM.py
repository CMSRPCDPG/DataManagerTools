#!/usr/bin/env python
## Browse DQM to extract RPC histograms
## Full documentation for the DQMJson is available in https://twiki.cern.ch/twiki/bin/view/CMS/DQMToJSON

import os, sys, urllib2, httplib, json, re
from ROOT import *
from array import array

class X509CertAuth(httplib.HTTPSConnection):
    ssl_key_file = None
    ssl_cert_file = None

    def __init__(self, host, *args, **kwargs):
        httplib.HTTPSConnection.__init__(self, host,
                                         key_file = X509CertAuth.ssl_key_file,
                                         cert_file = X509CertAuth.ssl_cert_file,
                                         **kwargs)

class X509CertOpen(urllib2.AbstractHTTPHandler):
    def default_open(self, req):
        return self.do_open(X509CertAuth, req)

def x509_params():
    key_file = cert_file = None

    res = os.popen("voms-proxy-info -p").read().strip()
    if res == '': os.system('voms-proxy-init -voms cms')
    res = os.popen("voms-proxy-info -p").read().strip()

    x509_path = res
    if x509_path and os.path.exists(x509_path):
        key_file = cert_file = x509_path

    if not key_file:
        x509_path = os.getenv("X509_USER_KEY", None)
        if x509_path and os.path.exists(x509_path):
            key_file = x509_path

    if not cert_file:
        x509_path = os.getenv("X509_USER_CERT", None)
        if x509_path and os.path.exists(x509_path):
            cert_file = x509_path

    if not key_file:
        x509_path = os.getenv("HOME") + "/.globus/userkey.pem"
        if os.path.exists(x509_path):
            key_file = x509_path
 
    if not cert_file:
        x509_path = os.getenv("HOME") + "/.globus/usercert.pem"
        if os.path.exists(x509_path):
            cert_file = x509_path

    if not key_file or not os.path.exists(key_file):
        print >>sys.stderr, "no certificate private key file found"
        sys.exit(1)

    if not cert_file or not os.path.exists(cert_file):
        print >>sys.stderr, "no certificate public key file found"
        sys.exit(1)

    print "Using SSL private key", key_file
    print "Using SSL public key", cert_file
    return key_file, cert_file

class DQMJSON:
    def __init__(self, server="https://cmsweb.cern.ch/dqm/offline"):
        self.server = server
        self.ident = "DQMToJson/1.0 python/%d.%d.%d" % sys.version_info[:3]
        self.runs = {}

        X509CertAuth.ssl_key_file, X509CertAuth.ssl_cert_file = x509_params()

    def retrieve(self, path):
        datareq = urllib2.Request('%s/%s' % (self.server, path))
        datareq.add_header('User-agent', self.ident)
        return urllib2.build_opener(X509CertOpen()).open(datareq).read()

    def retrieveJSON(self, query):
        #print '@@ Connectig to %s/data/json/%s' % (self.server, query)
        datareq = urllib2.Request('%s/data/json/%s' % (self.server, query))
        datareq.add_header('User-agent', self.ident)
        return eval(re.sub(r"\bnan\b", "0", urllib2.build_opener(X509CertOpen()).open(datareq).read()),
                    { "__builtins__": None }, {})

    def retrievePlot(self, query):
        #print '@@ Connecting to %s/jsrootfairy/archive/%s?jsroot=true' % (self.server, query)
        #datareq = urllib2.Request('%s/jsrootfairy/archive/%s?jsroot=true' % (self.server, query))
        datareq = urllib2.Request('%s/jsonfairy/archive/%s?rootcontent=1' % (self.server, query))
        datareq.add_header('User-agent', self.ident)
        return eval(re.sub(r"\bnan\b", "0", urllib2.build_opener(X509CertOpen()).open(datareq).read()),
                    {"__builtins__":None}, {})['hist']

    def getValueFromQuery(self, res, qry1, qry2):
        if qry1 not in res: return None
        if len(res[qry1]) == 0: return None
        for item in res[qry1]:
            if qry2 in item: return item[qry2]
        return None

    def loadRuns(self, match):
        res = self.getValueFromQuery(self.retrieveJSON('samples?match=%s' % (match)), 'samples', 'items')
        if res == None: return

        for x in res:
            #if x['type'] != 'offline_data': continue
            self.runs[int(x['run'])] = x['dataset']

    def getObjectInfo(self, run, hPath):
        dataset = self.runs[run]
        path = os.path.dirname(hPath)
        name = os.path.basename(hPath)
        res0 = self.retrieveJSON('archive/%d/%s/%s' % (run, dataset.strip('/'), path.strip('/')))
        if res0 == None or 'contents' not in res0: return None

        typeName, nentries, stats = None, None, None
        for x in res0['contents']:
            if 'obj' not in x or 'stats' not in x: continue
            if 'properties' not in x: continue
            p = x['properties']
            if 'kind' not in p: continue

            if x['obj'] == name:
                typeName, stats, nentries = p['type'], x['stats'], x['nentries']
                value = None
                if 'value' in x: value = x['value']
                break
        if typeName == None: return None
        if typeName == 'TPROF': typeName = 'TProfile'

        return {'stats':stats, 'nentries':nentries, 'type':typeName, 'name':name, 'path':path, 'value':value}

    def listDir(self, run, path):
        dataset = self.runs[run]
        res0 = self.retrieveJSON('archive/%d/%s/%s' % (run, dataset.strip('/'), path.strip('/')))
        if res0 == None or 'contents' not in res0: return None

        subdirs = [x['subdir'] for x in res0['contents'] if 'subdir' in x]
        objs = [x['obj'] for x in res0['contents'] if 'obj' in x]
        return subdirs, objs

    def getObjectJSON(self, run, hPath):
        dataset = self.runs[run]
        info = self.getObjectInfo(run, hPath)
        if not info:
            print hPath
            return "" 

        #res = self.getValueFromQuery(self.retrieveJSON('archive/%d/%s/%s?rootcontent=1' % (run, dataset.strip('/'), hPath.strip('/'))),
        #                             'contents', 'streamerinfo')
        #if res == None: return None
        #if len(res) == 0: return None
        #return self.streamerToObj(res, info['type'])

        res = self.retrievePlot('%d/%s/%s' % (run, dataset.strip('/'), hPath.strip('/')))
        return res

    def streamerToObj(self, streamer, typeName):
        ## This is not working somehow...
        arr = array('B')
        arr.fromstring(streamer.decode('hex'))
        bf = TBufferFile(TBufferFile.kRead, len(arr), arr, False)
        if typeName == 'TPROF': typeName = 'TProfile'
        obj = bf.ReadObject(eval(typeName+'.Class()'))

        return obj

    def buildObj(self, h, suffix=""):
        if 'type' not in h: return

        typeName = h["type"]
        hout = None
        if 'TH1' in typeName:
            contents = h['bins']['content']
            nbins = len(contents)
            xmin = h['xaxis']['first']['value']
            xmax = h['xaxis']['last']['value']

            hout = TH1F(h['stats']['name']+suffix, h['title'], nbins, xmin, xmax)
            for b in range(nbins):
                hout.SetBinContent(b+1, contents[b])
                hout.SetBinError(b, h['bins']['error'][b])

            entries = h['stats']['entries']
            mean = h['stats']['mean']['X']['value']
            rms = h['stats']['rms']['X']['value']

            hout.SetEntries(entries)
            stats = array('d', [entries, entries,
                                entries*mean, (rms*rms+mean*mean)*entries])
            hout.PutStats(stats)

            if 'labels' in h['xaxis'] and len(h['xaxis']['labels']) > 0:
                for b, label in enumerate([x['value'] for x in h['xaxis']['labels']]):
                    hout.GetXaxis().SetBinLabel(b+1, label)

        elif 'TH2' in typeName:
            contents = h['bins']['content']
            nbinsx = h['xaxis']['last']['id']
            xmin, xmax = h['xaxis']['first']['value'], h['xaxis']['last']['value']
            nbinsy = h['yaxis']['last']['id']
            ymin, ymax = h['yaxis']['first']['value'], h['yaxis']['last']['value']

            hout = TH2F(h['stats']['name']+suffix, h['title'], nbinsx, xmin, xmax, nbinsy, ymin, ymax)
            for bx in range(nbinsx):
                for by in range(nbinsy):
                    hout.SetBinContent(bx+1,by+1, contents[by][bx])

            entries = h['stats']['entries']
            xmean, ymean = h['stats']['mean']['X']['value'], h['stats']['mean']['Y']['value']
            xrms, yrms = h['stats']['rms']['X']['value'], h['stats']['rms']['Y']['value']
            hout.SetEntries(entries) 
            stats = array('d', [entries, entries,
                                entries*xmean, (xrms*xrms+xmean*xmean)*entries,
                                entries*ymean, (yrms*yrms+ymean*ymean)*entries])
            hout.PutStats(stats)

            if 'labels' in h['xaxis'] and len(h['xaxis']['labels']) > 0:
                for b, label in enumerate([x['value'] for x in h['xaxis']['labels']]):
                    hout.GetXaxis().SetBinLabel(b+1, label)
            if 'labels' in h['yaxis'] and len(h['yaxis']['labels']) > 0:
                for b, label in enumerate([y['value'] for y in h['yaxis']['labels']]):
                    hout.GetYaxis().SetBinLabel(b+1, label)


        return hout

if __name__ == "__main__":
    dqmJSON = DQMJSON()
    dqmJSON.loadRuns('/SingleMuon/Run2017.*')
    lastRun = 302526
    subdirs = dqmJSON.listDir(lastRun, '/RPC/AllHits/SummaryHistograms')
    hJSON = dqmJSON.getObjectJSON(lastRun, '/RPC/AllHits/SummaryHistograms/BxDistribution_Wheel_0')
    info = dqmJSON.getObjectInfo(lastRun, '/RPC/AllHits/SummaryHistograms/BxDistribution_Wheel_0')
    print hJSON
    print info['stats']['x']

