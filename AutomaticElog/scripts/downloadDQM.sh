#!/bin/bash

if [ $# != 4 ]; then
  echo "DQM root file downloader"
  echo "Usage: $0 ERA DATASET RUNNUMBER OUTDIR"
  echo "  ERA       : Run2017,HIRun2017,Commissioning2017,Cosmics2017,etc"
  echo "  DATASET   : Cosmics,SingleMu,SingleMuon,SingleMuon_0T,etc"
  echo "  RUNNUMBER : Run number"
  echo "  OUTDIR    : Output directory"
  exit 1
fi
ERA=$1
DATASET=$2
RUN=$3
OUTDIR=$4

## Define global variables
WORKDIR=`pwd`
DQMBASE="https://cmsweb.cern.ch/dqm/offline/data/browse/ROOT/OfflineData/$ERA/$DATASET"

. scripts/setupCERT.sh

[ -d $OUTDIR ] || mkdir -p $OUTDIR

## Download DQM
echo "@@@ Retrieving data from DQM @@@"
CURL="curl -s -L --cert $VOMS_PROXY --key $VOMS_PROXY -k"
DQMSUBDIR="$DQMBASE/`printf "%07dxx" $(($RUN/100))`"

$CURL $DQMSUBDIR -o $OUTDIR/DQMFileList.html
DQMFILE=""

for F in `cat $OUTDIR/DQMFileList.html | grep -o "DQM_V[0-9]\+_R0*${RUN}__[^><']*.root"`; do
  ## Loop over $DQMFILES but break to be safe for multiple matching
  DQMFILE=$F
  echo $DQMFILE
  break ## Loop over $DQMFILES but break to be safe for multiple matching
done

if [ _$DQMFILE == "_" ]; then
  echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
  echo "!!Cannot find DQM root file!!"
  echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
  exit 3
fi

## Now we are ready to download DQM.root
$CURL "$DQMSUBDIR/$DQMFILE" -o $OUTDIR/$DQMFILE

