#!/bin/bash

if [ $# != 2 ]; then
  echo "RPC simple e-log generator"
  echo "Usage: $0 DATATYPE RUNNUMBER"
  exit 1
fi
TYPE=$1
RUN=$2

if [ $TYPE != Cosmics ] && [ $TYPE != Collisions ]; then
  echo "DATATYPE must be Cosmics or Collisions"
  exit 1
fi

## Define global variables
WORKDIR=`pwd`
OUTDIR=$WORKDIR/cache/$RUN

if [ -d $OUTDIR ]; then
  echo "Erasing old files for run ${RUN}"
  rm -rf $OUTDIR
fi

mkdir -p $OUTDIR

URLNTL="https://rpcbackground.web.cern.ch/rpcbackground/Plots/GR2014"
URLWBM="https://cmswbm.cern.ch"

## Download NoiseTool pages
#echo "@@@ Retrieving data from NoiseTool @@@"
#if [ ! -f $OUTDIR/output_strips.json ]; then
#  ./scripts/downloadCMSWeb.sh $URLNTL/run${RUN}/output_strips.json $OUTDIR/output_strips.json &
#fi

## Download WBM JSON files
echo "@@@ Retrieving data from RPC WBM @@@"
if [ ! -f $OUTDIR/runSummary.json ]; then
  MENU="cmsdb/servlet/RPCRunSummary2?TopMenu=RPCRunSummary2&TopMenu2=ZeroPage"
  ./scripts/downloadCMSWeb.sh "$URLWBM/$MENU&Run=${RUN}" $OUTDIR/runSummary.json &

  MENU="cmsdb/servlet/RPCBackground?isSP=0&isHist=1"
  ./scripts/downloadCMSWeb.sh "$URLWBM/$MENU&SubMenu=0&Run=${RUN}" $OUTDIR/Brate.json
  ./scripts/downloadCMSWeb.sh "$URLWBM/$MENU&SubMenu=1&Run=${RUN}" $OUTDIR/BtoEnable.json &
  ./scripts/downloadCMSWeb.sh "$URLWBM/$MENU&SubMenu=2&Run=${RUN}" $OUTDIR/BtoDisable.json &
  ./scripts/downloadCMSWeb.sh "$URLWBM/$MENU&SubMenu=3&Run=${RUN}" $OUTDIR/BdeadStrip.json &
  ./scripts/downloadCMSWeb.sh "$URLWBM/$MENU&SubMenu=4&Run=${RUN}" $OUTDIR/Bdisabled.json &

  MENU="cmsdb/servlet/RPCBackground?isSP=1&isHist=1"
  ./scripts/downloadCMSWeb.sh "$URLWBM/$MENU&SubMenu=0&Run=${RUN}" $OUTDIR/Erate.json
  ./scripts/downloadCMSWeb.sh "$URLWBM/$MENU&SubMenu=1&Run=${RUN}" $OUTDIR/EtoEnable.json &
  ./scripts/downloadCMSWeb.sh "$URLWBM/$MENU&SubMenu=2&Run=${RUN}" $OUTDIR/EtoDisable.json &
  ./scripts/downloadCMSWeb.sh "$URLWBM/$MENU&SubMenu=3&Run=${RUN}" $OUTDIR/EdeadStrip.json &
  ./scripts/downloadCMSWeb.sh "$URLWBM/$MENU&SubMenu=4&Run=${RUN}" $OUTDIR/Edisabled.json &

  MENU="cmsdb/servlet/RPCBackground?isHist=0"
  ./scripts/downloadCMSWeb.sh "$URLWBM/$MENU&SubMenu=0&Run=${RUN}" $OUTDIR/rateTable.json
  ./scripts/downloadCMSWeb.sh "$URLWBM/$MENU&SubMenu=1&Run=${RUN}" $OUTDIR/toEnableTable.json &
  ./scripts/downloadCMSWeb.sh "$URLWBM/$MENU&SubMenu=2&Run=${RUN}" $OUTDIR/toDisableTable.json &
  ./scripts/downloadCMSWeb.sh "$URLWBM/$MENU&SubMenu=3&Run=${RUN}" $OUTDIR/deadStripTable.json &
  ./scripts/downloadCMSWeb.sh "$URLWBM/$MENU&SubMenu=4&Run=${RUN}" $OUTDIR/disabledTable.json &

  wait
fi

# Download DQM
URLDQM="https://cmsweb.cern.ch/dqm/offline/data/browse/ROOT/OfflineData"
DATASET=""
DATASET2=""
ERA=""
ERA2=""
if [ $TYPE == Cosmics ]; then
  DATASET="Cosmics"
  DATASET2="StreamExpressCosmics"
  if [ $RUN -le 262455 ]; then ## 262455 is the latest run visible in the DQM
    ERA="Run2015"
    ERA2="Commissioning2015"
  elif [ $RUN -le 263797 ]; then ## 263797 is the latest HI cosmic run visible in the DQM
    ERA="HIRun2015"
  elif [ $RUN -le 284177 ]; then
    ERA="Run2016"
    ERA2="Commissioning2016"
  elif [ $RUN -le 307554 ]; then
    ERA="Run2017"
    ERA2="Commissioning2017" 
  else  ## FIXME: similar elif have to be added after Run2019's
    ERA="Run2018"
    ERA2="Commissioning2018"
  fi
else
  DATASET="SingleMuon"
  DATASET2="SingleMuon_0T"
  if [ $RUN -le 251155 ]; then
    # NOTE : Dataset name /SingleMu/ is changed to /SingleMuon/ from Run2015B
    ERA="Run2015"
    DATASET="SingleMu"
  elif [ $RUN -le 261422 ]; then
    ERA="Run2015"
  elif [ $RUN -le 263757 ]; then
    ERA="HIRun2015"
    DATASET="HIOniaL1DoubleMu0"
  elif [ $RUN -le 284068 ]; then
    ERA="Run2016"
  elif [ $RUN -le 307082 ]; then
    ERA="Run2017"
    ERA2="Commissioning2017"
  else   ## FIXME: similar elif have to be added after Run2019's
    ERA="Run2018"
    ERA2="Commissioning2018"
  fi
fi
#./scripts/downloadDQM.sh $ERA $DATASET $RUN $OUTDIR
#[ $? -ne 0 ] && ./scripts/downloadDQM.sh $ERA $DATASET2 $RUN $OUTDIR
#[ $? -ne 0 -a _$ERA2 != _ ] && ./scripts/downloadDQM.sh $ERA2 $DATASET $RUN $OUTDIR
#[ $? -ne 0 -a _$ERA2 != _ -a _$DATASET2 != _ ] && ./scripts/downloadDQM.sh $ERA2 $DATASET2 $RUN $OUTDIR

echo "@@@ All files are ready. Start analysis @@@"
echo "@@@ Start analysis of $RUN @@@"
## Setup CMSSW
echo "@@@ Setting up CMSSW @@@"
cd /cvmfs/cms.cern.ch/slc6_amd64_gcc630/cms/cmssw/CMSSW_10_1_0
eval `scram runtime -sh`
cd $WORKDIR

python analyze.py $ERA $DATASET $RUN
