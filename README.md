# Automatic E-Log tool for RPC Data Managers

Up-to-date shift instructions can be found in the following links:
  * https://twiki.cern.ch/twiki/bin/viewauth/CMS/RPCOfflineShifts\_Run2
  * https://twiki.cern.ch/twiki/bin/view/CMS/RPCAutomaticElog

## Installation
  1. Log on a Console 3 or 4 in the CMS Centre.
  2. Open a terminal and run the following commands

```bash
ssh -XY <YOUR_AFS_LOGIN>@lxplus.cern.ch
cd /tmp/$USER # or any other directory with enough space. The output file can be large

wget https://gitlab.cern.ch/CMSRPCDPG/DataManagerTools/repository/archive.tar.gz?ref=v1.3 -O ar.tgz
tar xzf ar.tgz
mv DataManagerTools-v1.3-*/AutomaticElog ./
rm -rf DataManagerTools-v1.3-*
rm -f ar.tgz
```

Then you are ready to run the script.

```
./run.sh Collisions 302567
```

